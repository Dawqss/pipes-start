import {Pipe, PipeTransform} from "@angular/core";


@Pipe({
  name: 'shorten'
})
export class ShortenPipe implements PipeTransform {
  splitedValue : Array<string>;
  transform(value: any, limit?: number) {
    if (limit === undefined) {
      limit = 1;
    }
    let valueC: string = '';
    this.splitedValue = value.split(' ');
    for (const word of this.splitedValue) {
      if (word !== undefined) {
        valueC += word.substr(0, limit);
      }
    }
    return valueC;
  }
}